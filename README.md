# Desafio Data Engineering

Os scripts de resolução das questões do desafio se encontram em suas respectivas pastas. Usarei desse espaço para comentar um pouco sobre as questões.

Questão 1

Essa foi a mais difícil para mim, uma vez que não atuo diariamente com Python e meus conhecimentos nessa linguagem se resumem a cursos online e estudos por conta própria, porém, foi extremamente gratificante resolver um "problema do mundo real" utilizando de uma linguagem que não tenho pleno domínio, me apoiando em lógica e na minha capacidade de resolver problemas.
Infelizmente, por estar em um momento apertado de prazo no projeto atual, não tive muito tempo livre e creio que poderia ter desenvolvido melhor esse código.

Questão 2 

Consegui chegar a uma solução de maneira mais fácil para essa questão, uma vez que SQL está presente no meu dia a dia e é uma linguagem que gosto muito de trabalhar. Já havia encontrado problemas semelhantes no dia a dia de projeto, principalmente trabalhando com Hadoop Hive, utilizando da função Rank para encontrar registros mais recentes e arquitetar uma estratégia de carga, uma vez que nas versões em que trabalhei a função Update não estava ativa.


