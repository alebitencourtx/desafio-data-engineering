SELECT VENDEDOR.vendedor_nome, VENDA.venda_valor FROM(
SELECT
    venda_id,
    vendedor_id,
    venda_valor,
    venda_data,
    RANK() OVER (PARTITION BY
                     vendedor_id
                 ORDER BY
                     venda_valor DESC
                ) sales_rank
FROM TB_VENDA
  WHERE  YEAR(venda_data) = 2016
  ) VENDA
INNER JOIN TB_VENDEDOR VENDEDOR
ON VENDA.vendedor_id = VENDEDOR.vendedor_id
WHERE sales_rank = 1
;