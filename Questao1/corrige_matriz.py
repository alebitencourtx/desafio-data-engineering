import numpy as np

# GERA MATRIZ ESCADINHA
arr2D = np.array(
  [
    [11, 12, 78, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
    [10, 11, 12, 78, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [9, 10, 11, 12, 78, 1, 2, 3, 4, 5, 6, 7, 8],
    [8, 9, 10, 11, 12, 78, 1, 2, 3, 4, 5, 6, 7],
    [7, 8, 9, 10, 11, 12, 78, 1, 2, 3, 4, 5, 6],
    [6, 7, 8, 9, 10, 11, 12, 78, 1, 2, 3, 4, 5],
    [5, 6, 7, 8, 9, 10, 11, 12, 78, 1, 2, 3, 4],
    [4, 5, 6, 7, 8, 9, 10, 11, 12, 78, 1, 2, 3],
    [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 78, 1, 2],
    [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 78, 1],
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 78],
    [78, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
  ]
)
# APRESENTA MATRIZ
print('------------MATRIZ-----------')
linhas = arr2D[: , 0]
print(arr2D)

# ENCONTRA E APRESENTAO OS INDICES DOS MAIORES VALORES, POR LINHA
print('------------INDICES-----------')
indices = arr2D.argmax(axis = 1)
print(indices)

# ENCONTRA E APRESENTAO OS MAIORES VALORES
print('------------MAX-----------')
print(arr2D[np.arange(len(arr2D)), indices.squeeze()])

# CONTADOR GERADO PARA INCREMENTAR A LINHA DA MATRIZ
cont = 0

print('------------for-----------')
for i in indices:
  # SOMA OS 12 PRIMEIROS ELEMENTOS DA LINHA
  #print(arr2D[cont][0:-1].sum())
  # MAIOR VALOR DA LINHA
  #print(arr2D[np.arange(len(arr2D)) , indices.squeeze()][cont])
  # VERIFICA SE A SOMA DOS ELEMENTOS É IGUAL AO MAIOR VALOR DA LINHA
  correto = arr2D[cont][0:-1].sum() == arr2D[np.arange(len(arr2D)) , indices.squeeze()][cont]
  if correto == True:
    print('A linha ', cont + 1, ' veio correta:')  
    print(arr2D[cont])
  else:
    print('Linha corrigida')
    print(np.roll(arr2D[cont], -(i + 1)))
  cont = cont + 1